.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

==============================================
Contract: edit all lines' Date of Next Invoice
==============================================

* Edit all contract lines' Date of Next Invoice at once

Configuration
=============
\-

Usage
=====
* Open a contract where "Recurrence at line level?" is enabled
* In Recurring Invoices tab, Set "New Date of Next Invoice" and click "Set for All Lines"

Bug Tracker
===========
Bugs are tracked on `GitHub Issues
<https://github.com/tawasta/contract/issues>`_.

Credits
=======

Contributors
------------

* Jarmo Kortetjärvi <jarmo.kortetjarvi@tawasta.fi>
* Timo Talvitie <timo.talvitie@tawasta.fi>

Maintainer
----------

.. image:: https://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: https://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
