.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=====================================================
Contract Invoice Address and force Commercial Partner
=====================================================

Add invoice address to contracts and assign its commercial partner to an invoice
if needed with a setting.

Configuration
=============
Go to Invoice --> Configuration and select "Force commercial partner on contracts"
to set commercial partner to an invoice to be created.

Usage
=====
Select a contract and test its invoice creation. Use "Force commercial partner
on contracts" setting to see that the invoice gets the commercial partner of
an invoice address of contract.

Known issues / Roadmap
======================
\-

Credits
=======

Contributors
------------

* Valtteri Lattu <valtteri.lattu@tawasta.fi>
* Miika Nissi <miika.nissi@tawasta.fi>
* Timo Kekäläinen <timo.kekalainen@tawasta.fi>

Maintainer
----------

.. image:: https://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: https://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
